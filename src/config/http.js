import axios from 'axios';

function makeHTTP(options) {
  return options ? options.client || axios.create() : axios.create();
}

export default makeHTTP;
