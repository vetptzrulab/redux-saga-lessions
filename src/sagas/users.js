import { takeLatest, call, put } from 'redux-saga/effects';
import UserService from '../services/users';
import { GET_USERS, SET_USER } from '../store/user/action-types';

const userService = new UserService();

/**
 * Пример саги вочера
 */
export default function* watcherUsers() {
  yield takeLatest(GET_USERS, workerUsers);
}

/**
 * Пример саги: вочер/воркер
 */
// function* watcherUsers() {
//   while (true) {
//     yield take(GET_USERS, workerUsers);
//     const response = yield call(getUsers);
//     yield put({ type: SET_USER, payload: response.data || [] });
//   }
// }

export function* workerUsers() {
  try {
    yield put({ type: SET_USER, payload: [] });
    const users = yield call(userService.getList);
    yield put({ type: SET_USER, payload: users || [] });
  } catch (e) {
    console.error(e);
  }
}
