import { takeLatest, call, put } from 'redux-saga/effects';
import UserService from '../services/users';
import { GET_PROFILE, SET_PROFILE } from '../store/profile/action-types';

const userService = new UserService();

export default function* watcherProfile() {
  yield takeLatest(GET_PROFILE, workerProfile);
}

export function* workerProfile({ payload }) {
  try {
    yield put({ type: SET_PROFILE, payload: null });
    const response = yield call(userService.getProfile, payload);
    yield put({ type: SET_PROFILE, payload: response });
  } catch (e) {
    console.error(e);
  }
}
