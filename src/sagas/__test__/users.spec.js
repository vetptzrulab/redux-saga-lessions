import { call, put, fork, takeLatest } from 'redux-saga/effects';
import UserService from '../../services/users';
import * as actionTypesUsers from '../../store/user/action-types';
import * as actionTypesProfile from '../../store/profile/action-types';
import { workerUsers } from '../users';
import { workerProfile } from '../profile';

describe('Тестирование саги пользователей', () => {
  const userService = new UserService();
  it('Сага на получение списка пользователей', () => {
    const generator = workerUsers();
    expect(generator.next().value).toEqual(
      put({ type: actionTypesUsers.SET_USER, payload: [] })
    );
    expect(JSON.stringify(generator.next().value)).toEqual(
      JSON.stringify(call(userService.getList))
    );
    expect(generator.next().value).toEqual(
      put({ type: actionTypesUsers.SET_USER, payload: [] })
    );
  });

  it('Сага на получение профиля пользователя', () => {
    const id = 1;
    const generator = workerProfile({ payload: id });
    expect(generator.next().value).toEqual(
      put({ type: actionTypesProfile.SET_PROFILE, payload: null })
    );
    expect(JSON.stringify(generator.next().value)).toEqual(
      JSON.stringify(call(userService.getProfile, id))
    );
    expect(generator.next().value).toEqual(
      put({ type: actionTypesProfile.SET_PROFILE, payload: undefined })
    );
  });
});
