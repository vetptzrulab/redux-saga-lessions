import { fork, all } from 'redux-saga/effects';
import userSaga from './users';
import profileSaga from './profile';

export default function* rootSaga() {
  yield all([fork(userSaga), fork(profileSaga)]);
}
