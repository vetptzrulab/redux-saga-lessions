import React, { Component } from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './store/';
import watcherSaga from './sagas';
import List from './components/List';
import UserList from './containers/UserList';

// const store = createStore(rootReducer, applyMiddleware(thunk));
// dev tools middleware
const reduxDevTools =
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(sagaMiddleware),
    reduxDevTools
  )
);

const UList = UserList(List);

// then run the saga
sagaMiddleware.run(watcherSaga);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <UList />
      </Provider>
    );
  }
}

export default App;
