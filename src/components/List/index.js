import React from 'react';
import User from '../User/index';

const List = ({ users, getProfile }) => {
  return (
    <ul>
      {users && users.length > 0 ? (
        users.map(user => (
          <User key={user.id} user={user} getProfile={getProfile} />
        ))
      ) : (
        <li>Empty</li>
      )}
    </ul>
  );
};

export default List;
