import React from 'react';

const User = function({ user, getProfile }) {
  return (
    <li>
      <h5>
        {user.id}: {user.first_name} {user.last_name}
      </h5>
      <button
        onClick={() => {
          getProfile(user.id);
        }}
      >
        Detail
      </button>
    </li>
  );
};

export default User;
