import React from 'react';
import { connect } from 'react-redux';
import { GET_USERS } from '../../store/user/action-types';
import { GET_PROFILE } from '../../store/profile/action-types';

const UsersList = Component => {
  class WrappedComponent extends React.Component {
    componentDidMount() {
      this.props.getUsers();
      this.getProfile = this.getProfile.bind(this);
    }

    getProfile(id) {
      this.props.getProfile(id);
    }

    render() {
      return (
        <div>
          <button
            onClick={() => {
              this.props.getUsers();
            }}
          >
            Reload
          </button>
          <Component {...this.props} getProfile={this.getProfile} />
          <hr />
          <button
            onClick={() => {
              this.props.getUsers();
            }}
          >
            Reload
          </button>
        </div>
      );
    }
  }
  function mapStateToProps(state) {
    return {
      users: state.user
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
      getUsers: () => dispatch({ type: GET_USERS }),
      getProfile: userId => dispatch({ type: GET_PROFILE, payload: userId })
    };
  }
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(WrappedComponent);
};

export default UsersList;
