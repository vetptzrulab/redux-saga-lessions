import { SET_USER, RESET_USER } from './action-types';

const initialState = [];

export default function user(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case SET_USER:
      return payload;
    case RESET_USER:
      return initialState;
    default:
      return state;
  }
}
