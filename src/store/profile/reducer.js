import { SET_PROFILE, RESET_PROFILE } from './action-types';

const initialState = null;

export default function profile(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case SET_PROFILE:
      return payload;
    case RESET_PROFILE:
      return initialState;
    default:
      return state;
  }
}
