export const GET_PROFILE = 'get-profile';
export const SET_PROFILE = 'set-profile';
export const RESET_PROFILE = 'reset-profile';
