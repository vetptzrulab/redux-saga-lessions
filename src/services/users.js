import http from '../config/http';

export default class UserService {
  constructor(options) {
    this.client = http(options);
    this.getList = this.getList.bind(this);
    this.getProfile = this.getProfile.bind(this);
  }

  async getList() {
    return this.client
      .get('https://reqres.in/api/users/')
      .then(response => response.data.data);
  }

  async getProfile(id) {
    return this.client
      .get(`https://reqres.in/api/users/${id}`)
      .then(response => response.data.data);
  }
}
