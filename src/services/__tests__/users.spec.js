import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import UserService from '../users';

/**
 * Тесть сервиса пользователей
 */
describe('Проверка сервиса для работы с пользователями', () => {
  describe('Список пользователей', () => {
    const mock = new MockAdapter(axios);
    const userService = new UserService(mock);
    const USERS_RESPONSE = {
      page: 1,
      per_page: 1,
      total: 1,
      total_pages: 1,
      data: [
        {
          id: 1,
          first_name: 'George',
          last_name: 'Bluth',
          avatar:
            'https://s3.amazonaws.com/uifaces/faces/twitter/calebogden/128.jpg'
        }
      ]
    };
    mock.onGet('https://reqres.in/api/users/').reply(200, USERS_RESPONSE);
    let users;
    beforeEach(async () => {
      users = await userService.getList();
    });
    it('GET запрос за пользователями', async () => {
      expect(mock.history.get.length).toEqual(1);
    });
    it('Проверка данных (ответа сервера)', async () => {
      expect(users).toEqual(USERS_RESPONSE.data);
    });
  });
  // Получение пользователя по идентификатору
  describe('Получение профиля пользователя', () => {
    const mock = new MockAdapter(axios);
    const userService = new UserService(mock);
    const PROFILE_RESPONSE = {
      data: {
        id: 1,
        first_name: 'George',
        last_name: 'Bluth',
        avatar:
          'https://s3.amazonaws.com/uifaces/faces/twitter/calebogden/128.jpg'
      }
    };
    mock.onGet('https://reqres.in/api/users/1').reply(200, PROFILE_RESPONSE);
    let profile;
    beforeEach(async () => {
      profile = await userService.getProfile(1);
    });
    it('GET запрос за профилем', () => {
      expect(mock.history.get.length).toEqual(1);
    });
    it('Проверка данных (ответа сервера)', () => {
      expect(profile).toEqual(PROFILE_RESPONSE.data);
    });
  });
});
